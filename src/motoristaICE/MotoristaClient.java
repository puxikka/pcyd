package motoristaICE;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.zeroc.Ice.Application;
import com.zeroc.Ice.Communicator;
import com.zeroc.Ice.ObjectAdapter;
import com.zeroc.Ice.ObjectPrx;

import utils.OrganizadorRutasPrx;

public class MotoristaClient extends Application { 
	Communicator ic;
	ObjectAdapter adaptador;
	OrganizadorRutasPrx orgRutas;
	Scanner teclado;

	@Override
	public int run(String[] arg0) {
		ic = MotoristaClient.communicator();
		teclado = new Scanner(System.in);
		obtenerOrgRutas();
		if (orgRutas != null) {
			hacerOperaciones();
		} else {
			System.out.println("no se ha encontrado organizador IA");
		}
		teclado.close();
		return 0;

	}

	private void hacerOperaciones() {
		int opcion;
		String ruta;
		do {
			opcion = menu();
			switch (opcion) {
			case 1:
				ruta = orgRutas.getRuta();
				System.out.println(ruta);
				if (!ruta.contains("No hay rutas")) {
					System.out
							.println("*****************Press enter when the route has been finished*****************");
					teclado.nextLine();
					orgRutas.finalizarPedido(Integer.parseInt(ruta.split("->")[0].split("#")[1]));
				}
				break;
			case 0:
				break;
			default:
				System.out.println("Opci�n no v�lida");
			}

		} while (opcion != 0);
	}

	private int menu() {
		int opcion = 999;
		try {
			System.out.println("1.- Conseguir ruta");
			System.out.println("0.- Salir");
			System.out.print("selecione opcion: ");
			opcion = teclado.nextInt();
			teclado.nextLine();
			return opcion;
		} catch (InputMismatchException e) {
			teclado.nextLine();
			return -1;
		}
	}

	private void obtenerOrgRutas() {
		ObjectPrx object = ic.stringToProxy("OrganizadorRutas: tcp -h 127.0.0.1 -p 5000");
		orgRutas = OrganizadorRutasPrx.checkedCast(object);

	}

	public static void main(String args[]) {
		MotoristaClient cliente = new MotoristaClient();
		int status = cliente.main("cliente motorista", args);
		System.exit(status);
	}
}
